<?php
use brocoder\Fra\APKDomainsRotator\Database;
use brocoder\Fra\APKDomainsRotator\Logger;

require __DIR__ . '/../src/Config.php';

try {
    echo Database::get( $_SERVER[ 'REMOTE_ADDR' ] );
}
catch( Exception $e ) {
    Logger::error( $e->getMessage() );
}