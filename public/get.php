<?php
require __DIR__ . '/../src/Config.php';

use brocoder\Fra\APKDomainsRotator\Database;
use brocoder\Fra\APKDomainsRotator\Domains;
use brocoder\Fra\APKDomainsRotator\Logger;

try {
    if( isset( $_GET[ 'ref' ] ) ) {
        Database::insert( $_SERVER[ 'REMOTE_ADDR' ], $_GET[ 'ref' ], 3 * 60 );
    }
    
    $domains = new Domains( DOMAINS_LIST_PATH, GOOGLE_SAFE_BROWSING_API_KEY );
    $redirectUrl = "{$_SERVER[ 'REQUEST_SCHEME' ]}://{$domains->getClean()}{$_SERVER[ 'REQUEST_URI' ]}";
    header( "Location: {$redirectUrl}" );
    Logger::info( $_SERVER[ 'REMOTE_ADDR' ] . " redirected to {$redirectUrl}" );
}
catch( Exception $e ) {
    Logger::error( $e->getMessage() );
}