<?php
define( '__ROOT__', __DIR__ . '/..' );

define( 'DOMAINS_LIST_PATH', __ROOT__ . '/db/domains.txt' );
define( 'GOOGLE_SAFE_BROWSING_API_KEY', 'AIzaSyDpTE1M95YZXs8Il3gCHC6EXlyCqlYfE9w' );
define( 'TEST_DOMAIN_BLACKLISTED', 'apk-get-update.info' ); // this domain must be blacklisted!

ini_set( "log_errors", 1 );
ini_set( "error_log", __ROOT__ . '/logs/fatal.log' );

require_once __ROOT__ . '/vendor/autoload.php';
require_once __ROOT__ . '/src/Domains.php';
require_once __ROOT__ . '/src/Logger.php';
require_once __ROOT__ . '/src/Database.php';