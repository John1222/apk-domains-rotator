<?php
namespace brocoder\Fra\APKDomainsRotator;

use Predis\Client;

require_once __DIR__ . '/Config.php';

class Database
{
    private static $redis;
    
    public static function insert( string $ip, string $ref, int $expireSec = null )
    {
        if( is_null( $expireSec ) ) {
            self::getRedis()->set( $ip, $ref );
        }
        else {
            self::getRedis()->set( $ip, $ref, 'ex', $expireSec );
        }
    }

    /**
     * @param $ip
     * @return false|string
     */
    public static function get( string $ip )
    {
        $res = self::getRedis()->get( $ip );
        return is_null( $res ) ? false : $res;
    }

    private static function getRedis()
    {
        if( self::$redis == null ) {
            self::$redis = new Client();
        }
        return self::$redis;
    }
}