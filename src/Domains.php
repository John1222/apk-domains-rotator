<?php
namespace brocoder\Fra\APKDomainsRotator;

use brocoder\Fra\APKDomainsRotator\Exceptions\DomainsListEmptyException;
use brocoder\Fra\APKDomainsRotator\Exceptions\DomainsListNotFoundException;
use Brocoder\FileSystem\SFileLocked;
use Brocoder\GoogleSafeBrowsingAPI\GoogleSafeBrowsingAPI;
use Brocoder\GoogleSafeBrowsingAPI\GoogleSafeBrowsingAPIException;
use Exception;

require_once __DIR__ . '/Config.php';
require_once __ROOT__ . '/src/Exceptions/DomainsListEmptyException.php';
require_once __ROOT__ . '/src/Exceptions/DomainsListNotFoundException.php';

class Domains
{
    /**
     * @var string
     */
    private $domainsListPath;
    /**
     * @var GoogleSafeBrowsingAPI
     */
    private $GoogleSafeBrowsingAPI;

    /**
     * @param string $domainsListPath
     * @param string $GoogleSafeBrowsingAPIKey
     * @throws DomainsListEmptyException
     * @throws DomainsListNotFoundException
     */
    public function __construct( string $domainsListPath, string $GoogleSafeBrowsingAPIKey )
    {
        if( ! file_exists( $domainsListPath ) ) {
            throw new DomainsListNotFoundException( 'Domains list not found' );
        }
        if( empty( trim( file_get_contents( $domainsListPath ) ) ) ) {
            throw new DomainsListEmptyException( 'Domains list is empty' );
        }
        $this->domainsListPath = $domainsListPath;
        $this->GoogleSafeBrowsingAPI = new GoogleSafeBrowsingAPI( $GoogleSafeBrowsingAPIKey );
    }

    /**
     * Получаем список всех чистых доменов
     * 
     * @return array
     */
    public function getAllClean(): array
    {
        $result = [];
        foreach( SFileLocked::readAllAsLines( $this->domainsListPath ) as $domain ) {
            if( ! empty( trim( $domain ) ) ) {
                $result[] = $this->extractDomainFromURL( $domain );
            }
        }
        return $result;
    }

    /**
     * Получаем ближайший чистый домен
     * 
     * @return string
     */
    public function getClean(): string
    {
        return $this->extractDomainFromURL( SFileLocked::readLine( $this->domainsListPath, 0 ) );
    }
    
    private function extractDomainFromURL( string $URL )
    {
        return preg_replace( '/.*?([a-z0-9-_]*?\.[a-z0-9-_]*)(?:\/.*)?/i', '$1', $URL );
    }

    /**
     * Вычищаем из файла $domainsListPath все спалившиеся домены
     *
     * @return int  Количество вычищенных доменов
     * @throws GoogleSafeBrowsingAPIException
     */
    public function cleanOutBlacklisted(): int
    {
        $domainsClean = $this->getAllClean();
        $domainsBlacklisted = $this->GoogleSafeBrowsingAPI->isURLsBlacklisted( $domainsClean );
        $domainsCleanOnly = array_diff( $domainsClean, $domainsBlacklisted );
        SFileLocked::rewrite( $this->domainsListPath, implode( "\n", $domainsCleanOnly ) );
        return count( $domainsBlacklisted );
    }
}