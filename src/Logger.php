<?php
namespace brocoder\Fra\APKDomainsRotator;

use Brocoder\FileSystem\SFileLocked;
use brocoder\Fra\APKDomainsRotator\Tests\RotatorTester;
use Monolog\Handler\StreamHandler;

require_once __DIR__ . '/Config.php';

class Logger
{
    private const INFO_LOGS_PATH = __ROOT__ . '/logs/info.log';
    private const ERROR_LOGS_PATH = __ROOT__ . '/logs/errors.log';
    private const CRON_LOGS_PATH = __ROOT__ . '/logs/cron.log';

    /**
     * @var \Monolog\Logger
     */
    private static $loggerInfo;
    /**
     * @var \Monolog\Logger
     */
    private static $loggerErrors;
    /**
     * @var \Monolog\Logger
     */
    private static $loggerCron;

    public static function info( string $text )
    {
        if( self::$loggerInfo == null ) {
            self::$loggerInfo = new \Monolog\Logger( 'Rotator' );
            self::$loggerInfo->pushHandler( self::buildStreamHandler( self::INFO_LOGS_PATH ) );
        }
        self::$loggerInfo->info( $text );
    }

    public static function error( string $text )
    {
        if( self::$loggerErrors == null ) {
            self::$loggerErrors = new \Monolog\Logger( 'Rotator' );
            self::$loggerErrors->pushHandler( self::buildStreamHandler( self::ERROR_LOGS_PATH ) );
        }
        self::$loggerErrors->error( $text );
    }

    public static function cron( string $text )
    {
        if( self::$loggerCron == null ) {
            self::$loggerCron = new \Monolog\Logger( 'Cron' );
            self::$loggerCron->pushHandler( self::buildStreamHandler( self::CRON_LOGS_PATH ) );
        }
        self::$loggerCron->info( $text );
    }
    
    private static function buildStreamHandler( string $logsPath )
    {
        return new StreamHandler( $logsPath, \Monolog\Logger::DEBUG, true, null, true );
    }

    /**
     * @return bool|string
     */
    public static function getInfo()
    {
        return SFileLocked::readAll( self::INFO_LOGS_PATH );
    }

    /**
     * @return bool|string
     */
    public static function getError()
    {
        return SFileLocked::readAll( self::ERROR_LOGS_PATH );
    }

    /**
     * @return bool|string
     */
    public static function getCron()
    {
        return SFileLocked::readAll( self::CRON_LOGS_PATH );
    }
    
    public static function clearAll()
    {
        file_put_contents( self::INFO_LOGS_PATH, '' );
        file_put_contents( self::ERROR_LOGS_PATH, '' );
        file_put_contents( self::CRON_LOGS_PATH, '' );
    }
}