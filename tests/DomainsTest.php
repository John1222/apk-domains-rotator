<?php
namespace brocoder\Fra\APKDomainsRotator\Tests;

require_once __DIR__ . '/../src/Config.php';
require_once __DIR__ . '/RotatorTester.php';

use brocoder\Fra\APKDomainsRotator\Domains;
use brocoder\Fra\APKDomainsRotator\Exceptions\DomainsListEmptyException;
use brocoder\Fra\APKDomainsRotator\Exceptions\DomainsListNotFoundException;
use PHPUnit\Framework\AssertionFailedError;

class DomainsTest extends RotatorTester
{
    public function tearDown()
    {
        if( file_exists( DOMAINS_LIST_PATH ) ) {
            unlink( DOMAINS_LIST_PATH );
        }
    }
    
    private function setupDomainsList( array $domainsList )
    {
        file_put_contents( DOMAINS_LIST_PATH, implode( "\n", $domainsList ) );
    }

    /**
     * getAllClean и getNextClean должны преобразовывать домены в верный формат.
     * Пример: http://dom.com/folder/ -> dom.com.
     */
    public function testDomainFromURLExtraction()
    {
        $domainExpected = 'super-mega_domain.com';
        $domainsCorrupted = [
            "https://{$domainExpected}",
            "ftp://{$domainExpected}/",
            "http://{$domainExpected}/folder/path/",
            "://{$domainExpected}/folder/path/?getParam=123",
            "//{$domainExpected}///",
            "///{$domainExpected}/",
            $domainExpected,
            "$domainExpected/folder/to"
        ];

        file_put_contents( DOMAINS_LIST_PATH, implode( "\n", $domainsCorrupted ) );
        
        $domains = new Domains( DOMAINS_LIST_PATH, '' );
        $domainsClean = $domains->getAllClean();
        foreach( $domainsClean as $domainClean ) {
            $this->assertEquals( $domainExpected, $domainClean );
        }
        $this->assertEquals( $domainExpected, $domains->getClean() );
    }

    /**
     * На попытку инициализироваться с отсутствующим файлом-списком должно кидать исключение
     */
    public function testInitializationWithNonExistentList()
    {
        $this->expectException( DomainsListNotFoundException::class );
        new Domains( 'non_existent_domains_list.txt', GOOGLE_SAFE_BROWSING_API_KEY );
    }

    /**
     * На попытку инициализироваться с пустым файлом-списком должно кидать исключение
     */
    public function testInitializationWithEmptyList()
    {
        $isTestPassed = false;

        $emptyDomainsListPath = __DIR__ . '/empty_domains_list.txt';
        if( file_put_contents( $emptyDomainsListPath, '' ) === false ) {
            $this->markTestIncomplete( "Can't create file '{$emptyDomainsListPath}'" );
        }
        try {
            new Domains( $emptyDomainsListPath, GOOGLE_SAFE_BROWSING_API_KEY );
        }
        catch( DomainsListEmptyException $e ) {
            if( ! unlink( $emptyDomainsListPath ) ) {
                $this->markTestIncomplete( "Can't delete file '{$emptyDomainsListPath}'" );
            }
            $isTestPassed = true;
        }

        if( $isTestPassed ) {
            $this->assertTrue( true );
        }
        else {
            throw new AssertionFailedError( 'Failed asserting that exception of type DomainsListEmptyException" is thrown.' );
        }
    }

    /**
     * При инициализации Domains должно корректно подгружать из списка в софтину
     */
    public function testDomainsListInitialization()
    {
        $domainsList = $this->generateDomainsList( 20, 5 );
        $this->setupDomainsList( $domainsList );
        $domains = new Domains( DOMAINS_LIST_PATH, GOOGLE_SAFE_BROWSING_API_KEY );
        $this->assertArraySubset( $domains->getAllClean() ,$domainsList, true, 'Domains initialized incorrectly' );
    }

    /**
     * По запросу должно отдавать первый чистый домен
     */
    public function testGettingFirstCleanDomain()
    {
        $domainsList = $this->generateDomainsList();
        $this->setupDomainsList( $domainsList );
        $domains = new Domains( DOMAINS_LIST_PATH, GOOGLE_SAFE_BROWSING_API_KEY );
        $this->assertEquals( $domainsList[ 0 ], $domains->getClean() );
    }

    /**
     * По запросу должно чекать все домены и сейвить чистые в файло, убирая паленые. Пустые строки должны
     * игнорироваться, в противном случае АПИ выплюнет ошибку проверки урла.
     */
    public function testCleaningOutBlacklistedDomains()
    {
        $blacklistedDomainsCount = 3;
        $emptyDomainsCount = 2;
        $domainsList = $this->generateDomainsList( 20, $blacklistedDomainsCount, $emptyDomainsCount );
        $this->setupDomainsList( $domainsList );
        
        $domains = new Domains( DOMAINS_LIST_PATH, GOOGLE_SAFE_BROWSING_API_KEY );
        $this->assertTrue(
            in_array( TEST_DOMAIN_BLACKLISTED, $domains->getAllClean() ),
            "Blacklisted domain '" . TEST_DOMAIN_BLACKLISTED . "' don't found in list of clean domains"
        );
        
        $this->assertEquals( $blacklistedDomainsCount, $domains->cleanOutBlacklisted() );
        
        $this->assertFalse(
            in_array( TEST_DOMAIN_BLACKLISTED, $domains->getAllClean() ),
            "Blacklisted domain '" . TEST_DOMAIN_BLACKLISTED . "' must be cleaned out from the list of clean domains"
        );
        $this->assertNotEquals( TEST_DOMAIN_BLACKLISTED, $domains->getClean() );
        
        $expectedCountOfDomains = count( $domainsList ) - $blacklistedDomainsCount - $emptyDomainsCount;
        $this->assertCount(
            $expectedCountOfDomains, $domains->getAllClean(),
            "Domains count must be equals to {$expectedCountOfDomains}"
        );
    }
}