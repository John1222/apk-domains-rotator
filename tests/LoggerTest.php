<?php
namespace brocoder\Fra\APKDomainsRotator\Tests;

require_once __DIR__ . '/../src/Config.php';
require_once __DIR__ . '/RotatorTester.php';

use brocoder\Fra\APKDomainsRotator\Database;
use brocoder\Fra\APKDomainsRotator\Domains;
use brocoder\Fra\APKDomainsRotator\Exceptions\DomainsListEmptyException;
use brocoder\Fra\APKDomainsRotator\Exceptions\DomainsListNotFoundException;
use brocoder\Fra\APKDomainsRotator\Logger;

class LoggerTest extends RotatorTester
{
    public function testInfoLogging()
    {
        Logger::clearAll();
        $this->assertEmpty( Logger::getInfo() );
        
        $testLogRecord = 'info test log';
        Logger::info( $testLogRecord );
        $this->assertRegExp( "/{$testLogRecord}/", Logger::getInfo() );
    }

    public function testErrorsLogging()
    {
        Logger::clearAll();
        $this->assertEmpty( Logger::getInfo() );

        $testLogRecord = 'error test log';
        Logger::error( $testLogRecord );
        $this->assertRegExp( "/{$testLogRecord}/", Logger::getError() );
    }
    
    public function testCronLogging()
    {
        Logger::clearAll();
        $this->assertEmpty( Logger::getCron() );

        $testLogRecord = 'cron test log';
        Logger::cron( $testLogRecord );
        $this->assertRegExp( "/{$testLogRecord}/", Logger::getCron() );
    }
    
    public function testClearing()
    {
        $this->testInfoLogging(); // делаем тестовую запись

        Logger::clearAll();
        $this->assertEmpty( Logger::getInfo() );
    }
}