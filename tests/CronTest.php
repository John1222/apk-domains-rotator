<?php
namespace brocoder\Fra\APKDomainsRotator\Tests;

require_once __DIR__ . '/../src/Config.php';
require_once __DIR__ . '/RotatorTester.php';

use brocoder\Fra\APKDomainsRotator\Database;
use brocoder\Fra\APKDomainsRotator\Domains;
use brocoder\Fra\APKDomainsRotator\Exceptions\DomainsListEmptyException;
use brocoder\Fra\APKDomainsRotator\Exceptions\DomainsListNotFoundException;

class CronTest extends RotatorTester
{
    /**
     * cron.php при запуске из консоли должен вычищать паленые домены
     */
    public function testDomainsCleaningOut()
    {
        $blacklistedDomainsCount = 2;
        $domainsList = $this->generateDomainsList( 20, $blacklistedDomainsCount );
        
        if( ! file_put_contents( DOMAINS_LIST_PATH, implode( "\n", $domainsList ) ) ) {
            $this->markTestIncomplete( "Can't create file '" . DOMAINS_LIST_PATH . '\'' );
        }
        $domains = new Domains( DOMAINS_LIST_PATH, GOOGLE_SAFE_BROWSING_API_KEY );
        $this->assertTrue(
            in_array( TEST_DOMAIN_BLACKLISTED, $domains->getAllClean() ),
            "Blacklisted domain '" . TEST_DOMAIN_BLACKLISTED . "' don't found in list of clean domains"
        );

        exec( 'php "' . __ROOT__ . '/cron.php"' );

        $this->assertFalse(
            in_array( TEST_DOMAIN_BLACKLISTED, $domains->getAllClean() ),
            "Blacklisted domain '" . TEST_DOMAIN_BLACKLISTED . "' must be cleaned out from the list of clean domains"
        );
        $this->assertNotEquals( TEST_DOMAIN_BLACKLISTED, $domains->getClean() );

        $expectedCountOfDomains = count( $domainsList ) - $blacklistedDomainsCount;
        $this->assertCount(
            $expectedCountOfDomains, $domains->getAllClean(),
            "Domains count must be $expectedCountOfDomains"
        );
    }
}