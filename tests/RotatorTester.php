<?php
namespace brocoder\Fra\APKDomainsRotator\Tests;

use brocoder\Fra\APKDomainsRotator\Logger;
use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/../src/Config.php';

class RotatorTester extends TestCase
{
    private static $cleanDomainsListBackup;

    public static function setUpBeforeClass()
    {
        if( file_exists( DOMAINS_LIST_PATH ) ) {
            self::$cleanDomainsListBackup = file_get_contents( DOMAINS_LIST_PATH );
        }
        Logger::clearAll();
    }

    public static function tearDownAfterClass()
    {
        if( is_null( self::$cleanDomainsListBackup ) && file_exists( self::$cleanDomainsListBackup ) ) {
            unlink( self::$cleanDomainsListBackup );
        }
        else {
            file_put_contents( DOMAINS_LIST_PATH, self::$cleanDomainsListBackup );
        }
    }
    
    protected function isWindows()
    {
        return strpos( PHP_OS, 'WIN' ) !== false;
    }

    protected function generateRandomString( int $length = 10 )
    {
        return substr(
            str_shuffle( str_repeat( $x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
                ceil( $length / strlen( $x ) ) ) ),
            1,
            $length
        );
    }

    protected function generateDomainsList( int $clean = 20, int $blacklisted = 0, $empty = 0 ): array
    {
        $result = [];
        for( $i = 0; $i < $clean; ++$i ) {
            $result[] = $this->generateRandomString() . '.com';
        }
        for( $i = 0; $i < $blacklisted; ++$i ) {
            $result[] = TEST_DOMAIN_BLACKLISTED;
        }
        for( $i = 0; $i < $empty; ++$i ) {
            $result[] = "";
        }
        shuffle( $result );
        return $result;
    }
}