<?php
namespace brocoder\Fra\APKDomainsRotator\Tests;

use brocoder\Fra\APKDomainsRotator\Database;

require_once __DIR__ . '/../src/Config.php';
require_once __DIR__ . '/RotatorTester.php';

class DatabaseTest extends RotatorTester
{
    /**
     * Добавление записи
     */
    public function testInsert()
    {
        $ip = $this->generateIP();
        $ref = $this->generateRandomString();

        Database::insert( $ip, $ref );
        $this->assertEquals( $ref, Database::get( $ip ) );
    }

    /**
     * Добавленная запись должна исчезать через заданный промежуток времени
     */
    public function testInsertWithExpiry()
    {
        $ip = $this->generateIP();
        $ref = $this->generateRandomString();

        Database::insert( $ip, $ref, 3 );
        
        sleep( 1 );
        $this->assertEquals( $ref, Database::get( $ip ) );
        
        sleep( 3 );
        $this->assertFalse( Database::get( $ip ) );
    }
    
    private function generateIP()
    {
        return preg_replace_callback(
            '|%s|',
            function () {
                return mt_rand( 1, 255 );
            },
            '%s.%s.%s.%s'
        );
    }
}