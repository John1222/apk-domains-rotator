<?php
namespace brocoder\Fra\APKDomainsRotator\Tests;

require_once __DIR__ . '/../src/Config.php';
require_once __DIR__ . '/RotatorTester.php';

use brocoder\Fra\APKDomainsRotator\Database;
use brocoder\Fra\APKDomainsRotator\Domains;
use brocoder\Fra\APKDomainsRotator\Exceptions\DomainsListEmptyException;
use brocoder\Fra\APKDomainsRotator\Exceptions\DomainsListNotFoundException;

class RedirectTest extends RotatorTester
{
    public function testHTTPRedirects()
    {
        $this->makeTestRedirect( 'http' );
    }

    /* Решил не тестить https. Потому что если пашет по http, то знач пашет и остальное, а результат отработки https
    зависит не от меня, а от настройки сертификатов, привязанных, к тому же, к конкретным доменам
    public function testHTTPSRedirects()
    {
        $this->makeTestRedirects( 'https' );
    }
    */

    /**
     * Тестим редирект с get.php на чистый домен. Тест проверяет:
       * редирект на другой домен строго с URL (схема, путь, query) из первичного запроса
       * запись в базу связки ip-ref
       * выдачу из базы заданного ref'а по ипу запрашивающего
     *
     * @param string $scheme
     * @throws DomainsListEmptyException
     * @throws DomainsListNotFoundException
     */
    private function makeTestRedirect( string $scheme )
    {
        $domainsList = $this->generateDomainsList( 3, 1 );
        if( ! file_put_contents( DOMAINS_LIST_PATH, implode( "\n", $domainsList ) ) ) {
            $this->markTestIncomplete( "Can't create file '" . DOMAINS_LIST_PATH . '\'' );
        }
        $domains = new Domains( DOMAINS_LIST_PATH, GOOGLE_SAFE_BROWSING_API_KEY );

        $host = ( $this->isWindows() ) ? 'apk-domains-rotator.com' : 'localhost'; // на винде - это мой серв
        for( $redirectIdx = 0; $redirectIdx < 5; ++$redirectIdx ) {
            $refExpected = $this->generateRandomString();
            $url = sprintf(
                '%s://%s/get.php?ref=%s&%s=%s',
                $scheme, $host, $refExpected, $this->generateRandomString(), $this->generateRandomString()
            );
            $response = $this->sendGETRequest( $url, true );
            
            $this->assertRegExp( '/HTTP\\/1\\.\\d 302 Found/', $response, 'It\'s not a redirect' );

            $parsedUrl = parse_url( $url );
            $locationExpected = preg_quote(
                "{$parsedUrl[ 'scheme' ]}://{$domains->getClean()}{$parsedUrl[ 'path' ]}?{$parsedUrl[ 'query' ]}",
                '/'
            );
            $this->assertRegExp(
                "/location: {$locationExpected}/i", $response,
                "Location URL '{$locationExpected}' is invalid"
            );

            $this->assertEquals( $refExpected, $this->getRefFromDatabaseByLocalIP(), 'Ref wasn\'t setup' );
            $this->assertEquals(
                $refExpected,
                $this->sendGETRequest( "{$scheme}://{$host}/get_ref.php", false ),
                'get_ref.php returned invalid ref'
            );
        }
    }
    
    private function sendGETRequest( string $url, bool $returnHeaders = true ): string
    {
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_HEADER, $returnHeaders );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, false );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        $output = curl_exec( $ch );
        curl_close( $ch );
        return $output;
    }

    private function getRefFromDatabaseByLocalIP()
    {
        foreach( [ '127.0.0.1', 'localhost', '::1' ] as $possibleLocalIP ) {
            $ref = Database::get( $possibleLocalIP );
            if( $ref !== false ) {
                return $ref;
            }
        }
        return false;
    }
}