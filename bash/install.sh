#!/bin/bash

WWW_DIR='/var/www/html'
CRON_JOB='*/1 * * * *'

YELLOW='\033[0;33m'
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m'

printYellowText()
{
    printf "${YELLOW}${1}${NC}"
}

printRedText()
{
    printf "${RED}${1}${NC}"
}

printGreenText()
{
    printf "${GREEN}${1}${NC}"
}

isCronJobInstalled()
{
    if [[ $(crontab -l | grep "${WWW_DIR}/cron.php") ]]
	then
	  true
	else
	  false
	fi
}

isNginxOrApacheInstalled()
{
    if [[ $(dpkg --get-selections | grep -E "apache|nginx") ]]
	then
	  true
	else
	  false
	fi
}

isPHPInstalled()
{
    if [[ $(dpkg --get-selections | grep "^php7.2") ]]
	then
	  true
	else
	  false
	fi
}

isComposerInstalled()
{
    if [[ $(dpkg --get-selections | grep "^composer") ]]
	then
	  true
	else
	  false
	fi
}

isComposerDependenciesInstalled()
{
  if [ -d "${WWW_DIR}/vendor" ]
  then
    true
  else
    false
  fi
}

isRedisInstalled()
{
    if [[ $(dpkg --get-selections | grep "^redis") ]]
	then
	  true
	else
	  false
	fi
}

cd ${WWW_DIR}

# rights
chmod -R 777 ./

sudo apt update

# delete web-servers
if isNginxOrApacheInstalled
then
  printYellowText "Nginx or Apache already installed. Removing it...\n"
  sudo apt-get purge 'nginx*' -y
  sudo apt-get purge 'apache*' -y
fi

# install nginx
printGreenText "Installing nginx... "
sudo apt-get install nginx -y > /dev/null
if isNginxOrApacheInstalled
then
  printGreenText "done\n"
else
  printRedText "failed. Please, check installation logs in /var/log/apt/term.log\n"
  exit
fi
sudo ufw allow 'Nginx HTTP'
default=''
default+='server {
  root '
default+=${WWW_DIR}
default+=';
    listen 80 default_server;
    listen [::]:80 default_server;
    server_name server_domain_or_IP;

    index index.php index.html index.htm index.nginx-debian.html;
	
    rewrite ^(.*)$ /public/$1 break;

    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php7.2-fpm.sock;
    }
}
'
echo "$default" > /etc/nginx/sites-available/default
service nginx restart

# PHP 7.2 (with all dependencies)
if isPHPInstalled
then
  printYellowText "PHP 7.2 already installed. Skipped.\n"
else
  printGreenText "Installing php 7.2... "
  sudo apt-get install python-software-properties -y > /dev/null
  sudo add-apt-repository ppa:ondrej/php -y > /dev/null
  sudo apt-get update > /dev/null
  sudo apt-get install php-pear php7.2-fpm php7.2-xml php7.2-simplexml php7.2-mbstring php7.2-curl php7.2-zip php7.2-imagick -y > /dev/null
  if isPHPInstalled
    then
	  printGreenText "done\n"
	else
	  printRedText "failed. Please, check installation logs in /var/log/apt/term.log\n"
	  exit
  fi
fi

# composer installation
if isComposerInstalled
then
  printYellowText "Composer already installed. Skipped.\n"
else
  printGreenText "Installing composer... "
  apt-get install composer -y > /dev/null
  if isComposerInstalled
  then
	printGreenText "done\n"
  else
	printRedText "failed. Please, check installation logs in /var/log/apt/term.log\n"
	exit
  fi
fi

# check scripts
if ! [ -d "${WWW_DIR}/src" ] ||
   ! [ -f "${WWW_DIR}/composer.json" ]; then
  mkdir -p ${WWW_DIR}
  printf "It seems you are not uploaded rotator scripts on folder ${WWW_DIR}. Please, fix it and try again\n"
  exit
fi

# composer dependencies installation
if isComposerDependenciesInstalled
then
  printYellowText "Composer dependencies already installed. Skipped\n"
else
  printGreenText "Installing composer dependencies..."
  composer install
  if isComposerDependenciesInstalled
  then
    printGreenText "done\n"
  else
    printRedText "failed\n"
	exit
  fi
fi

# cron
if isCronJobInstalled
then
  printYellowText "Cron job already exists. Skipped.\n"
else
  printGreenText "Installing cron job... "
  crontab -l | { cat; echo "${CRON_JOB} sudo -u www-data php ${WWW_DIR}/cron.php"; } | crontab -
  if isCronJobInstalled
  then
    printGreenText "done\n"
  else
	printRedText "failed\n"
    exit
  fi
fi

# redis
if isRedisInstalled
then
  printYellowText "Redis already exists. Skipped.\n"
else
  printGreenText "Installing Redis... "
  apt-get install redis-server -y
  redis-cli config set stop-writes-on-bgsave-error no
  redis-cli config set appendonly no
  redis-cli config set save ""
  redis-cli config rewrite
  systemctl start redis
  if isRedisInstalled
  then
    printGreenText "done\n"
  else
	printRedText "failed\n"
    exit
  fi
fi

# certbot
sudo apt-get install software-properties-common -y
sudo add-apt-repository ppa:certbot/certbot -y
sudo apt-get update
sudo apt-get install python-certbot-nginx -y