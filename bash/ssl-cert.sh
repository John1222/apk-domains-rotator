#!/bin/bash

read -p "$(tput setaf 2)[optional] Enter domain for SSL-cert: $(tput sgr 0)" domain
if ! [ -z "${domain}" ]; then
  sudo certbot --nginx --noninteractive -d ${domain}
fi