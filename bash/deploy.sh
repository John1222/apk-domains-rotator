#!/bin/bash

WWW_DIR='/var/www/html'

mkdir -p ${WWW_DIR} && cd ${WWW_DIR}

# update
sudo apt update

# git
sudo apt-get install git

# cloning the repo
sudo git clone https://brocoder1@bitbucket.org/brocoder_team/apk-domains-rotator.git .

# rights for all folder
chmod -R 777 ./

# install environment
sudo ${WWW_DIR}/bash/install.sh

# test
sudo ${WWW_DIR}/bash/test.sh

# ssl-cert (optional)
sudo ${WWW_DIR}/bash/ssl-cert.sh