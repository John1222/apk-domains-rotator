<?php
require_once __DIR__ . '/src/Config.php';

use brocoder\Fra\APKDomainsRotator\Domains;
use brocoder\Fra\APKDomainsRotator\Logger;

try {
    $domains = new Domains( DOMAINS_LIST_PATH, GOOGLE_SAFE_BROWSING_API_KEY );
    $countOfCleanedOutDomains = $domains->cleanOutBlacklisted();
    Logger::cron( count( $domains->getAllClean() ) . " domains was checked and {$countOfCleanedOutDomains} cleaned out" );
}
catch( Exception $e ) {
    Logger::cron( $e->getMessage() );
}